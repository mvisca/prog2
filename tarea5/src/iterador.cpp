/* 5119163 */

/**
@file iterador.cpp
@author Mario Visca
@date 30-04-2018
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "../include/iterador.h"

struct nodo_iter{
  char *str;
  nodo_iter *sig;
  nodo_iter *ant;
};

struct rep_iterador{
  nodo_iter *inicio;
  nodo_iter *final;
  nodo_iter *actual;
  nat n;
};

iterador_t crear_iterador(){
  iterador_t iter = new rep_iterador;
  iter->inicio = iter->final = iter->actual = NULL;
  iter->n = 0;
  return iter;
}

void agregar_a_iterador(char *t, iterador_t &i){
  nodo_iter *nuevo = new nodo_iter;
  nuevo->str = t;
  nuevo->sig = NULL;
  nuevo->ant = i->final;

  if (i->final == NULL){
    assert(i->inicio == NULL);
    i->inicio = nuevo;
  }else{
    assert(i->inicio != NULL);
    i->final->sig = nuevo;
  }
  i->final = nuevo;
  i->n += 1;
}

void reiniciar_iter(iterador_t &i){
  if (i->inicio != NULL){
    i->actual = i->inicio;
  }
}

void avanzar_iter(iterador_t &i){
  if ((i->actual == NULL) || !hay_siguiente_en_iter(i)){
    i->actual = NULL;
  }else{
    i->actual = i->actual->sig;
  }
}

char *actual_iter(iterador_t &i){
  assert(esta_definida_actual(i));
  return (i->actual->str);
}

bool hay_siguiente_en_iter(iterador_t i){
  assert(esta_definida_actual(i));
  return (i->actual->sig != NULL);
}

bool esta_definida_actual(iterador_t i){
  return (i->actual != NULL);
}

cadena_t iterador_a_cadena(iterador_t i){
  cadena_t cad = crear_cadena();
  if (i != NULL){
  nodo_iter *partida = i->actual;
  reiniciar_iter(i);
  while (i->actual != NULL){
    char *str = new char[strlen(actual_iter(i)) + 1];
    strcpy(str, actual_iter(i));
    insertar_al_final(crear_info(0, str), cad);
    avanzar_iter(i);
  }
  i->actual = partida;
  }
  return cad;
}

void liberar_iterador(iterador_t &i){
  if (i != NULL){
  nodo_iter *tmp;
  while (i->inicio != NULL){
    tmp = i->inicio;
    i->inicio = i->inicio->sig;
    delete[] tmp->str;
    delete tmp;
  }
  delete i;
  }
}
