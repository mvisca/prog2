/* 5119163 */

/**
@file binario.cpp
@author Mario Visca
@date 30-04-2018
*/

#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/binario.h"
#include "../include/iterador.h"
#include "../include/uso_tads.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#define max(x,y) ((x > y) ? x:y)

struct rep_binario {
  info_t dato;
  rep_binario *izq;
  rep_binario *der;
};

binario_t crear_binario() {
  return NULL;
}

/*
O(n)
Justificacion:
  Si planteo la formula de t(n) para el caso no base:
    - T(n) = T(n_izq) + T(n - n_izq - 1) + k
  En el caso de un arbol perfectamente balanceado n es impar,
  entonces T(n) = 2T(n/2) + k, por el "Teorema Maestro(T(n)=aT(n/b) + f(n^c))", a=2, b=2, c=0 y logb(a) > c en consecuencia la función tiene orden O(n^(logb(a))) que es O(n).

LQQD.
Otra manera es ver que es una recorrida en orden, y eso es O(n).
*/

static binario_t cadena2ABB(localizador_t &loc, cadena_t cad, nat longitud){
  if (longitud <= 0){
    return NULL;
  }else{
    binario_t izq = cadena2ABB(loc, cad, longitud/2);
    binario_t raiz = new rep_binario;
    raiz->dato = copia_info(info_cadena(loc, cad));
    raiz->izq = izq;
    loc = siguiente(loc, cad);
    raiz->der = cadena2ABB(loc, cad, longitud - longitud/2 -1);
    return raiz;
  }
}

binario_t crear_balanceado(cadena_t cad){// O(n)
  nat nodos = longitud(cad); // O(n)
  localizador_t loc = inicio_cadena(cad); // O(1)
  binario_t ret = cadena2ABB(loc, cad, nodos); // O(n)
  return ret;
}

info_t remover_mayor(binario_t &b){// O(log n)
  assert(!es_vacio_binario(b));
  info_t res;
  if(b->der == NULL){
    res = b->dato;
    binario_t izq = b->izq;
    delete (b);
    b = izq;
  } else {
    res = remover_mayor(b->der);
  }
  return res;
}

bool insertar_en_binario(info_t i, binario_t &b){// O(log n)
  bool insertado = false;
  if(b == NULL){
    b = new rep_binario;
    b->dato = i;
    b->izq = b->der = NULL;
    insertado = true;
  } else {
    if (strcmp(frase_info(i), frase_info(b->dato)) < 0){
      insertado = insertar_en_binario(i, b->izq);
    } else if (strcmp(frase_info(i), frase_info(b->dato)) > 0){
      insertado = insertar_en_binario(i, b->der);
    }
  }
  return insertado;
}

bool remover_de_binario(const char *t, binario_t &b){// O(log n)
  bool ret = false;
  if (b == NULL){
    // Caso base
    ret = false;
  } else {
    if (strcmp(t, frase_info(b->dato)) < 0) {
      // Borro el subAB izq.
      ret = remover_de_binario(t, b->izq);
    } else if (strcmp(t, frase_info(b->dato)) > 0){
      // Borro el subAB der.
      ret = remover_de_binario(t, b->der);
    } else {// O(log n)
      // Entro aca solo si t == b->dato
      if (b->der == NULL){// O(1)
        // Si el nodo donde estoy parado no tiene subAB der.
        // guardo el subAB izq. borro el actual y rengancho.
        binario_t aux = b->izq;
        liberar_info(b->dato);
        delete b;
        b = aux;
        ret = true;
      } else if (b->izq == NULL){// O(1)
        // Si el nodo donde estoy parado no tiene subAB izq.
        // guardo el subAB der. borro el actual y rengancho
        binario_t aux = b->der;
        liberar_info(b->dato);
        delete b;
        b = aux;
        ret = true;
      } else {// O(log n)
        // Libero el dato en cuestion para almacenar el dato del mayor
        // que retorna remover_mayor.
        liberar_info(b->dato);
        b->dato = remover_mayor(b->izq);
        ret = true;
      }
    }
  }
  return ret;
}

void liberar_binario(binario_t &b){// O(n)
  if (b == NULL){
    return;
  } else {
    // Libero en post-orden
    liberar_binario(b->izq);
    liberar_binario(b->der);
    liberar_info(b->dato);
    delete b;
    b = NULL;
    return;
  }
}

bool es_vacio_binario(binario_t b){// O(1)
  return (b == NULL);
}

static int cumple_AVL(binario_t b){// O(n)
  if (b == NULL){
    // Caso base
    return 0;
  }
  // Chequeo el sub arbol izq.
  int subAB_izq = cumple_AVL(b->izq);
  if (subAB_izq == -1){
    return -1;
  }
  // Chequeo el sub arbol der.
  int subAB_der = cumple_AVL(b->der);
  if (subAB_der == -1){
    return -1;
  }
  if (abs(subAB_izq - subAB_der) > 1){
    // Si no se cumple la condición de AVL retorno -1
      return -1;
  }
  // Retorno la máxima altura entre los sub arboles.
  return (max(subAB_izq, subAB_der) + 1);
}

bool es_AVL(binario_t b){// O(n)
  int ret = cumple_AVL(b); // O(n)
  return (ret < 0) ? false:true;
}

info_t raiz(binario_t b){// O(1)
  assert(!es_vacio_binario(b));
  return b->dato;
}

binario_t izquierdo(binario_t b){// O(1)
  assert(!es_vacio_binario(b));
  return b->izq;
}

binario_t derecho(binario_t b){// O(1)
  assert(!es_vacio_binario(b));
  return b->der;
}

binario_t buscar_subarbol(const char *t, binario_t b){// O(log n)
  binario_t loc;
  if (b == NULL){
    return NULL;
  } else {
    if (strcmp(t, frase_info(b->dato)) < 0){
      loc = buscar_subarbol(t, b->izq);
    } else if (strcmp(t, frase_info(b->dato))> 0){
      loc = buscar_subarbol(t, b->der);
    } else {
      loc = b;
    }
  }
  return loc;
}

nat altura_binario(binario_t b){// O(n)
  if (b == NULL){
    return 0;
  } else {
    return max(altura_binario(b->izq), altura_binario(b->der)) + 1;
  }
}

nat cantidad_binario(binario_t b){// O(n)
  if (b == NULL){
    return 0;
  } else {
    return cantidad_binario(b->izq) + cantidad_binario(b->der) + 1;
  }
}

static binario_t kesimo_aux(nat k, nat &cont, binario_t b){// O(n)
  // Que el cont se pase por referencia me permite llevar bien la cuenta de
  // cuantos nodos voy visitando.
  if (b == NULL){
    // Caso base
    return NULL;
  }
  binario_t loc;
  // Busco en el subAB izq.
  loc = kesimo_aux(k, cont, b->izq);
  if (loc == NULL){
    // Si no encontre el k esimo
    // debo incrementar el contador
    cont++;
    if (cont == k){
      // Si el contador == k, entonces estoy en el nodo a retornar.
      loc = b;
    }
  }
  if (loc == NULL){
    // Si aun no lo encontre busco en el subAB der.
    loc = kesimo_aux(k, cont, b->der);
  }
  return loc;
}

info_t kesimo_en_binario(nat k, binario_t b){// O(n)
  assert((1<= k )&& (k <= cantidad_binario(b)));
  binario_t loc;
  nat cont=0;
  loc = kesimo_aux(k, cont, b);
  return loc->dato;
}

static void to_list(cadena_t &c, binario_t b){// O(n)
  if(b == NULL){
    return;
  } else {
    // Copio en orden.
    to_list(c, b->izq);
    insertar_al_final(copia_info(b->dato), c);
    to_list(c, b->der);
  }
}

cadena_t linealizacion(binario_t b){// O(n)
  cadena_t cad = crear_cadena();
  if (b == NULL){
    return cad;
  } else {
    to_list(cad, b);
  }
  return cad;
}

binario_t filtrado(int clave, binario_t b){// O(n)
  binario_t copia_izq, copia_der, copia_raiz;
  if (b == NULL){
    // Caso base
    return NULL;
  } else {
    // Filtro el subAB izq.
    copia_izq = filtrado(clave, b->izq);
    // Filtro el subAB der.
    copia_der = filtrado(clave, b->der);
    if (numero_info(b->dato) < clave){
      // Copio el dato en la nueva raiz y enlazo los subAB's.
      copia_raiz = new rep_binario;
      copia_raiz->dato = copia_info(b->dato);
      copia_raiz->izq = copia_izq;
      copia_raiz->der = copia_der;
    }else if(copia_der == NULL){// O(1)
      // Si no hay nada en el subAB der. retorno el izq.
      return copia_izq;
    } else if(copia_izq == NULL){// O(1)
      // Si no hay nada en el subAB izq. retorno el der.
      return copia_der;
    } else {//O(log n)
      // Si ambos subAB's tienen hijos la nueva raiz tiene que almacenar
      // el dato del mayor en el subAB izq. y luego remuevo el mismo.
      copia_raiz = new rep_binario;
      copia_raiz->dato = remover_mayor(copia_izq); // O(log n)
      // enlazo la raiz con los subAB's.
      copia_raiz->izq = copia_izq;
      copia_raiz->der = copia_der;
    }
    return copia_raiz;
  }
}

static void imprimir_aux(int lvl, binario_t b){// O(n)
  if (es_vacio_binario(b)){
    // Caso base.
    printf("\n");
  } else {
    // Imprimo en orden inverso.
    imprimir_aux(lvl+1, b->der);
    for(int i=0; i<lvl; i++) printf("-");
    char *texto = info_a_texto(b->dato);
    printf("%s", texto);
    imprimir_aux(lvl+1, b->izq);
    delete []texto;
  }
}

void imprimir_binario(binario_t b){// O(n)
  imprimir_aux(0, b);
}
