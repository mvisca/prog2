/* 5119163 */

/**
@file cola_prioridad.cpp
@author Mario Visca
@date 15-06-2018
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "../include/cola_prioridad.h"


struct Nodo{
  prio_t prioridad;
  info_t dato;
};

struct rep_cola_prioridad{
  Nodo **heap;
  int *prioridades;
  unsigned int tamanio;
  unsigned int elementos;
  unsigned int sig_hueco;
  unsigned int max_prioridad;
};

typedef rep_cola_prioridad *cola_prioridad_t;

cola_prioridad_t crear_cp(unsigned int tamanio, prio_t max_prioridad){
  cola_prioridad_t cp = new  rep_cola_prioridad;
  cp->max_prioridad = max_prioridad;
  cp->sig_hueco = 0;
  cp->tamanio = tamanio;
  cp->elementos = 0;
  cp->heap = new Nodo*[tamanio];
  for (unsigned int i=0; i< tamanio; i++){
    cp->heap[i] = NULL;
  }
  cp->prioridades = new int[max_prioridad + 1];
  for (prio_t i=0; i < max_prioridad +1; i++){
    cp->prioridades[i] = -1;
  }
  return cp;
}

static void flotar(unsigned int indice, cola_prioridad_t &cp){
  if (indice == 0){
    return;
  } else {
    unsigned int ind_padre = (indice - 1)/2;
    if (cp->heap[ind_padre]->prioridad > cp->heap[indice]->prioridad){
      Nodo *aux = cp->heap[ind_padre];
      cp->heap[ind_padre] = cp->heap[indice];
      cp->heap[indice] = aux;
      cp->prioridades[cp->heap[ind_padre]->prioridad] = ind_padre;
      cp->prioridades[cp->heap[indice]->prioridad] = indice;
      indice = ind_padre;
      flotar(indice, cp);
    } else {
      return;
    }
  }
}

static bool tiene_hijoD(unsigned int indice, cola_prioridad_t cp){
  if (2*indice +1 >= cp->tamanio)
    return false;
  else
    return (cp->heap[2*indice + 1] != NULL);
}

static bool tiene_hijoI(unsigned int indice, cola_prioridad_t cp){
  if (2*indice +2 >= cp->tamanio)
    return false;
  else
    return (cp->heap[2*indice + 2] != NULL);
}

static prio_t prioridad(unsigned int indice, cola_prioridad_t cp){
  return cp->heap[indice]->prioridad;
}

static void hundir(unsigned int indice, cola_prioridad_t &cp){
  if (!tiene_hijoD(indice, cp)){
    return;
  }
  unsigned int indice_hijo;
  if (tiene_hijoD(indice, cp)  && tiene_hijoI(indice, cp)){
    if (prioridad(2*indice + 1, cp) < prioridad(2*indice + 2, cp)){
      indice_hijo = 2*indice + 1;
    } else {
      indice_hijo = 2*indice + 2;
    }
  } else if (tiene_hijoD(indice, cp)){
      indice_hijo = 2*indice + 1;
  } else {
    return;
  }

  if (prioridad(indice, cp) > prioridad(indice_hijo, cp)){
    Nodo *aux = cp->heap[indice];
    cp->heap[indice] = cp->heap[indice_hijo];
    cp->heap[indice_hijo] = aux;
    cp->prioridades[cp->heap[indice]->prioridad] = indice;
    cp->prioridades[cp->heap[indice_hijo]->prioridad] = indice_hijo;

    indice = indice_hijo;
    hundir(indice, cp);
  }
}

void insertar_en_cp(info_t i, prio_t p, cola_prioridad_t &cp){
	assert((!es_llena_cp(cp)) && (!hay_prioridad(p,cp)) && (p <= max_prioridad(cp)));
  Nodo *nodo = new Nodo;
  nodo->prioridad = p;
  nodo->dato = i;
  cp->heap[cp->sig_hueco] = nodo;
  cp->prioridades[p] = (cp->sig_hueco);
  flotar(cp->sig_hueco, cp);
  cp->elementos++;
  cp->sig_hueco++;
}

static unsigned int obtener_indice(prio_t p, cola_prioridad_t cp){
  return (cp->prioridades[p]);
}

void priorizar(prio_t p, cola_prioridad_t &cp){
	assert(hay_prioritario(p, cp) && !hay_prioritario(p/2));
  prio_t nueva_prio = p/2;
  cp->heap[obtener_indice(p, cp)]->prioridad = p/2;
  cp->prioridades[nueva_prio] = cp->prioridades[p];
  cp->prioridades[p] = -1;
  flotar(obtener_indice(nueva_prio, cp), cp);
}



void despriorizar(prio_t p, cola_prioridad_t &cp){
	assert(hay_prioritario(p, cp) && !hay_prioridad((p+max_prioridad(cp))/2));
  prio_t nueva_prio = (p + cp->max_prioridad)/2;
  cp->heap[obtener_indice(p, cp)]->prioridad = nueva_prio;
  cp->prioridades[nueva_prio] = cp->prioridades[p];
  cp->prioridades[p] = -1;
  hundir(obtener_indice(nueva_prio, cp), cp);
}

void eliminar_prioritario(cola_prioridad_t &cp){
	assert(!es_vacia_cp(cp));
  if (cp->elementos == 1){
    cp->prioridades[cp->heap[0]->prioridad] = -1;
    liberar_info(cp->heap[0]->dato);
    delete cp->heap[0];
    cp->heap[0] = NULL;
    cp->elementos--;
    cp->sig_hueco--;
  } else {
    prio_t indice_ultimo = cp->sig_hueco - 1;
    cp->prioridades[cp->heap[0]->prioridad] = -1;
    liberar_info(cp->heap[0]->dato);
    delete cp->heap[0];
    cp->heap[0] = cp->heap[indice_ultimo];
    cp->heap[indice_ultimo] = NULL;
    cp->prioridades[cp->heap[0]->prioridad] = 0;
    hundir(0, cp);
    cp->elementos--;
    cp->sig_hueco--;
  }
}

void liberar_cp(cola_prioridad_t &cp){
  for (unsigned int i=0; i < cp->tamanio; i++){
    if (cp->heap[i] != NULL){
      liberar_info(cp->heap[i]->dato);
      delete cp->heap[i];
    }
  }
  delete[] cp->heap;
  delete[] cp->prioridades;
  delete cp;
  cp = NULL;
}


bool es_vacia_cp(cola_prioridad_t cp){
  return (cp->elementos == 0);
}

bool es_llena_cp(cola_prioridad_t cp){
  return (cp->elementos == cp->tamanio);
}

bool hay_prioridad(prio_t p, cola_prioridad_t cp){
  if (p > cp->max_prioridad){
    return false;
  } else{
    return (cp->prioridades[p] >= 0);
  }
}

info_t prioritario(cola_prioridad_t cp){
	assert(!es_vacia_cp(cp));
  return (cp->heap[0]->dato);
}

prio_t prioridad_prioritario(cola_prioridad_t cp){
	assert(!es_vacia_cp(cp));
  return (cp->heap[0]->prioridad);
}

prio_t max_prioridad(cola_prioridad_t cp){
  return (cp->max_prioridad);
}

