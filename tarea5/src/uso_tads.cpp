/* 5119163 */

/**
@file uso_tads.cpp
@author Mario Visca
@date 15-06-2018
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "../include/uso_tads.h"
#include "../include/cola_binarios.h"
#include "../include/cola_prioridad.h"

cadena_t mayores(cadena_t cad, unsigned int k, unsigned int max){
	assert(k > 0);
  cola_prioridad_t cp = crear_cp(k, max);
  localizador_t cursor = inicio_cadena(cad);
  while (es_localizador(cursor)){
    if (numero_info(info_cadena(cursor, cad)) >= 0){
      prio_t paux = numero_info(info_cadena(cursor, cad));
      if (!hay_prioridad(paux, cp) && (paux <= max)){
        if (es_llena_cp(cp) && paux > prioridad_prioritario(cp)){
          eliminar_prioritario(cp);
          info_t aux = copia_info(info_cadena(cursor, cad));
          insertar_en_cp(aux, paux, cp);
        } else if(!es_llena_cp(cp)){
          info_t aux = copia_info(info_cadena(cursor, cad));
          insertar_en_cp(aux, paux, cp);
        }
      }
    }
  cursor = siguiente(cursor, cad);
  }
  cadena_t ret = crear_cadena();
  while (!es_vacia_cp(cp)){
      insertar_al_final(copia_info(prioritario(cp)), ret);
      eliminar_prioritario(cp);
  }
  liberar_cp(cp);
  return ret;
}

iterador_t inverso_de_iter(nat a, nat b, iterador_t &it){
  assert((1 <= a) && (a <= b));
  
  iterador_t ret = crear_iterador();
  reiniciar_iter(it);
  if (!esta_definida_actual(it)){
    return ret;
  }
  
  nat cant_elem = 1;
  while (hay_siguiente_en_iter(it)){
    cant_elem++;
    avanzar_iter(it);
  }
  reiniciar_iter(it);  
  if (a > cant_elem){
    return ret;
  }
  if (b > cant_elem){
    b = cant_elem;
  }
  for (nat i=b; i >= a; i--){
    reiniciar_iter(it);
    nat ind = 1;
    while (ind < i){
      avanzar_iter(it);
      ind++;
    }
    char *tmp = new char[strlen(actual_iter(it)) + 1];
    strcpy(tmp, actual_iter(it));
    agregar_a_iterador(tmp, ret);
  }  
  reiniciar_iter(it);
  reiniciar_iter(ret);

  return ret;
}

static void ABB2iter(iterador_t &it, char *c1, char *c2, binario_t b){
  if (b == NULL){
    return;
  }

  if (strcmp(c2, frase_info(raiz(b))) > 0){
    ABB2iter(it, c1, c2, derecho(b));
  }  
  if ((strcmp(c1, frase_info(raiz(b))) <= 0) && (strcmp(c2, frase_info(raiz(b))) >= 0)){
    char *tmp = new char[strlen(frase_info(raiz(b))) + 1];
    strcpy(tmp, frase_info(raiz(b)));
    agregar_a_iterador(tmp, it);
  }
  if (strcmp(c1, frase_info(raiz(b))) < 0){
    ABB2iter(it, c1, c2, izquierdo(b));
  }
}

iterador_t rango_en_conjunto(char *primero, char *ultimo, conjunto_t c){
  iterador_t ret = crear_iterador();
  iterador_t iter_conj = iterador_conjunto(c);
  cadena_t cad_conj = iterador_a_cadena(iter_conj);
  binario_t abb_conj = crear_balanceado(cad_conj);


  ABB2iter(ret, primero, ultimo, abb_conj);
  liberar_iterador(iter_conj);
  liberar_cadena(cad_conj);
  liberar_binario(abb_conj);

  return ret;
}


static void encolar_por_nivel(cola_binarios_t colas[],nat nivel, binario_t nodo){
  if (nodo == NULL){
    return;
  }else{
    encolar(nodo, colas[nivel]);
    encolar_por_nivel(colas, nivel+1, izquierdo(nodo));
    encolar_por_nivel(colas, nivel+1, derecho(nodo));
  }
}

void imprimir_por_niveles(binario_t b){
  if (b != NULL){
    int niveles = altura_binario(b);
    cola_binarios_t colas[niveles];
    for (int i=0; i<niveles; i++){
      colas[i] = crear_cola_binarios();
    }
    encolar_por_nivel(colas, 0, b);
  
    for (int i=(niveles-1); i >= 0; i--){
      cola_binarios_t cola = colas[i];
      while (!es_vacia_cola_binarios(cola)){
        printf("%s ", frase_info(raiz(frente(cola))));
        desencolar(cola);
      }
      if (i >0)
        printf("\n");
      liberar_cola_binarios(cola);
    }
  }
}

bool esta_ordenada_por_frase(cadena_t cad){
  bool res = true;
  if (!es_vacia_cadena(cad)){
    localizador_t loc = inicio_cadena(cad);
    while (res && es_localizador(siguiente(loc, cad))){
      localizador_t sig_loc = siguiente(loc, cad);
      if (strcmp(frase_info(info_cadena(loc, cad)), frase_info(info_cadena(sig_loc, cad))) >= 0)
        res = false;
      else
        loc = siguiente(loc, cad);
    }
  }
  return res;
}

bool pertenece(int i, cadena_t cad){
  localizador_t loc = siguiente_clave(i, inicio_cadena(cad), cad);
  return (loc != NULL);
}

nat longitud(cadena_t cad){
  nat len = 0;
  localizador_t loc = inicio_cadena(cad);
  while (es_localizador(loc)){
    len++;
    loc = siguiente(loc, cad);
  }
  
  return len;
}

bool son_iguales(cadena_t c1, cadena_t c2){
  if (es_vacia_cadena(c1) && es_vacia_cadena(c2)){
    return true;
  } else {
    localizador_t loc_c1 = inicio_cadena(c1);
    localizador_t loc_c2 = inicio_cadena(c2);
    // Avanzo de a 1, en ambas cadenas a la vez.
    // Ambos localizadores deben de ser validos.
    while (es_localizador(loc_c1) && es_localizador(loc_c2)){
      int n_c1 = numero_info(info_cadena(loc_c1, c1));
      int n_c2 = numero_info(info_cadena(loc_c2, c2));
      char *f_c1 = frase_info(info_cadena(loc_c1, c1));
      char *f_c2 = frase_info(info_cadena(loc_c2, c2));
      if ((n_c1 != n_c2) || (strcmp(f_c1, f_c2) != 0)){
        // si los datos numericos o las frases son distintas en los
        // localizadores actuales => no son iguales, retorno.
        return false;
      }
      // Avanzo ambos localizadores a la vez.
      loc_c1 = siguiente(loc_c1, c1);
      loc_c2 = siguiente(loc_c2, c2);
    }
    // Si el while se ejecuto completamente, si los localizadores
    // resultantes son == NULL, entonces las cadenas son iguales.
    return (loc_c1 == NULL) && (loc_c2 == NULL) ;
  }
}

void bubble_sort(localizador_t final, cadena_t cad){
  if (inicio_cadena(cad) == final_cadena(cad)){
    // si el final como el inicio apuntan al mismo nodo:
    //  - o bien la cadena es vacia => retorno, no hay nada para ordenar;
    //  - o bien hay un elemento => retorno, no hay nada para ordenar;
    return;
  }
  if (!es_localizador(final)){
    // condicion de parada. Si llegue a NULL de moviendome
    // desde la cola a la cabeza retorno.
    return;
  }

  localizador_t loc = inicio_cadena(cad);
  while (loc != final){
    if (numero_info(info_cadena(loc, cad)) >
        numero_info(info_cadena(siguiente(loc, cad), cad))){
      // Si el dato num siguiente es menor, debo intercambiarlos
      // Ej: [(2, i), (1,j)] => [(1, j), (2, i)]
      intercambiar(loc, siguiente(loc, cad), cad);
    }
    // Sino avanzo al siguiente
    loc = siguiente(loc, cad);
  }
  // llamo recursivamente con el anterior del final, dejo el ultimo elemento
  // fuera, ya que no hay elementos mas grandes que el luego del while.
  bubble_sort(anterior(final, cad), cad);
}


cadena_t concatenar(cadena_t c1, cadena_t c2){
  cadena_t c3 = segmento_cadena(inicio_cadena(c1), final_cadena(c1), c1);
  cadena_t sgm = segmento_cadena(inicio_cadena(c2), final_cadena(c2), c2);
  insertar_segmento_despues(sgm, final_cadena(c3), c3);
  liberar_cadena(sgm);
  return c3;
}

void ordenar(cadena_t &cad){
  if (!es_vacia_cadena(cad)){
    bubble_sort(final_cadena(cad), cad);
  }
}

void cambiar_todos(int original, int nuevo, cadena_t &cad){
  localizador_t loc = siguiente_clave(original, inicio_cadena(cad), cad);
  while (es_localizador(loc)) {
    char *frase_orig = new char[strlen(frase_info(info_cadena(loc, cad))) + 1];
    strcpy(frase_orig, frase_info(info_cadena(loc, cad)));
    info_t tmp = info_cadena(loc, cad);
    cambiar_en_cadena(crear_info(nuevo, frase_orig), loc, cad);
    liberar_info(tmp);
    loc = siguiente_clave(original, loc, cad);
  }
}

cadena_t subcadena(int menor, int mayor, cadena_t cad){
  assert(pertenece(menor, cad) && pertenece(mayor, cad));
  cadena_t ret = crear_cadena();
  // Obtengo el primer loc. donde aparece el menor. Punto de partida
  localizador_t loc = siguiente_clave(menor, inicio_cadena(cad), cad);
  while (es_localizador(loc) && (numero_info(info_cadena(loc, cad)) <= mayor)){
    insertar_al_final(copia_info(info_cadena(loc, cad)), ret);
    loc = siguiente(loc, cad);
  }
  return ret;
}

