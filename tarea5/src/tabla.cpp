/* 5119163 */

/**
@file tabla.cpp
@author Mario Visca
@date 15-06-2018
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "../include/tabla.h"

struct Nodo{
  int clave;
  char *texto;
  Nodo *sig;
  Nodo *ant;
};

struct rep_tabla{
  nat tamanio;
  nat disponibles;
  nat elementos;
  Nodo **imagen;
};

static int hash(int clave, nat tamanio){
  return (clave % tamanio);
}


tabla_t crear_tabla(nat tamanio){
  tabla_t tabla = new rep_tabla;
  tabla->elementos = 0;
  tabla->tamanio = tabla->disponibles = tamanio;
  tabla->imagen = new Nodo*[tamanio];
  for(nat i=0; i<tamanio; i++){
    tabla->imagen[i] = NULL;
  }
  return tabla;
}


static Nodo *localizar_en_tabla(int clave, tabla_t t){
  assert(existe_asociacion(clave, t));
  int indice = hash(clave, t->tamanio);
  Nodo *cursor = t->imagen[indice];
  while (cursor->clave != clave){
    cursor = cursor->sig;
  }
  return cursor;
}

void asociar_en_tabla(int clave, char *valor, tabla_t &t){
  assert(!esta_llena_tabla(t));
  int indice = hash(clave, t->tamanio);
  if (existe_asociacion(clave, t)){
    Nodo *tmp = localizar_en_tabla(clave, t);
    delete[] tmp->texto;
    tmp->texto = valor;
  } else {
    Nodo *nodo = new Nodo;
    nodo->clave = clave;
    nodo->texto = valor;
    nodo->sig = t->imagen[indice];
    nodo->ant = NULL;
    if (t->imagen[indice] == NULL){
      t->disponibles--;
    } else{
      t->imagen[indice]->ant = nodo;
    }
    t->imagen[indice] = nodo;
    t->elementos++;
  }
}

void eliminar_de_tabla(int clave, tabla_t &t){
  assert(existe_asociacion(clave, t));
  int indice = hash(clave, t->tamanio);
  Nodo *nodo = t->imagen[indice];
  while (nodo->clave != clave){
    nodo = nodo->sig;
  }
  delete[] nodo->texto;
  if ((nodo->ant == NULL) & (nodo->sig == NULL)){
    t->imagen[indice] = NULL;
  } else {
    if (nodo->ant == NULL){
      nodo->sig->ant = nodo->ant;
      t->imagen[indice] = nodo->sig;
    } else if (nodo->sig == NULL){
      nodo->ant->sig = NULL;
    } else{
      nodo->sig->ant = nodo->ant;
      nodo->ant->sig = nodo->sig;
    }
  }
  delete nodo;
  t->elementos--;

  if (t->imagen[indice] == NULL){
    t->disponibles++;
  }
}


static void borrar_nodos(Nodo* &nodo){
  Nodo *tmp;
  while (nodo != NULL){
    tmp = nodo;
    nodo = nodo->sig;
    delete[] tmp->texto;
    delete tmp;
  }
}

void liberar_tabla(tabla_t &t){
  for(nat i=0; i<t->tamanio; i++){
    borrar_nodos(t->imagen[i]);
  }
  delete[] t->imagen;
  delete t;
  t = NULL;
}

bool existe_asociacion(int clave, tabla_t t){
  int indice = hash(clave, t->tamanio);
  bool ret;
  if (t->imagen[indice] == NULL){
    ret = false;
  } else {
    Nodo *aux = t->imagen[indice];
    bool existe = false;
    while ((aux != NULL) & !existe){
      if (aux->clave == clave){
        existe = true;
      } else {
        aux = aux->sig;
      }
    }
    ret = existe;
  }
  return ret;
}

char *valor_en_tabla(int clave, tabla_t t){
  assert(existe_asociacion(clave, t));
  int indice = hash(clave, t->tamanio);
  Nodo *tmp = t->imagen[indice];
  while (tmp->clave != clave){
    tmp = tmp->sig;
  }
  return tmp->texto;
}

bool esta_llena_tabla(tabla_t t){
  return (t->elementos == t->tamanio);
}
