/* 5119163 */

/**
@file pila.cpp
@author Mario Visca
@date 30-04-2018
*/

#include <stdlib.h>
#include <assert.h>

#include "../include/pila.h"

struct rep_pila{
  int tope;
  int cota;
  char **array;
};

pila_t crear_pila(int tamanio){
  pila_t pila = new rep_pila;
  pila->array = new char*[tamanio];
  pila->tope = 0;
  pila->cota = tamanio;
  return pila;
}

void apilar(char *t, pila_t &p){
  if (!es_llena_pila(p)){
    p->array[p->tope] = t;
    p->tope += 1;
  }
}

void desapilar(pila_t &p){
  if (!es_vacia_pila(p)){
    delete[] p->array[p->tope-1];
    p->tope -= 1;
   }
}

void liberar_pila(pila_t &p){
  for (int i=0; i<p->tope; i++){
    delete[] p->array[i];
  }
  delete[] p->array;
  delete p;
  p = NULL;
}

bool es_vacia_pila(pila_t p){
  return (p->tope == 0);
}

bool es_llena_pila(pila_t p){
  return (p->tope == p->cota);
}

char *cima(pila_t p){
  assert(!es_vacia_pila(p));
  return p->array[p->tope -1 ];
}

