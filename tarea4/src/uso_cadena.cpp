/* 5119163 */

/**
@file uso_cadena.cpp
@author Mario Visca
@date 10-03-2018
*/

#include "../include/uso_cadena.h"

#include <stddef.h>
#include <string.h>
#include <assert.h>


bool pertenece(int i, cadena_t cad){
  localizador_t loc = siguiente_clave(i, inicio_cadena(cad), cad);
  return (loc != NULL);
}

nat _len_recursive(localizador_t loc, cadena_t cad){
  if (loc == NULL){
    return 0;
  } else {
    return 1 + _len_recursive(siguiente(loc, cad), cad);
  }
}

nat longitud(cadena_t cad){
  nat len;
  len = _len_recursive(inicio_cadena(cad), cad);
  return len;
}

bool son_iguales(cadena_t c1, cadena_t c2){
  if (es_vacia_cadena(c1) && es_vacia_cadena(c2)){
    return true;
  } else {
    localizador_t loc_c1 = inicio_cadena(c1);
    localizador_t loc_c2 = inicio_cadena(c2);
    // Avanzo de a 1, en ambas cadenas a la vez.
    // Ambos localizadores deben de ser validos.
    while (es_localizador(loc_c1) && es_localizador(loc_c2)){
      int n_c1 = numero_info(info_cadena(loc_c1, c1));
      int n_c2 = numero_info(info_cadena(loc_c2, c2));
      char *f_c1 = frase_info(info_cadena(loc_c1, c1));
      char *f_c2 = frase_info(info_cadena(loc_c2, c2));
      if ((n_c1 != n_c2) || (strcmp(f_c1, f_c2) != 0)){
        // si los datos numericos o las frases son distintas en los
        // localizadores actuales => no son iguales, retorno.
        return false;
      }
      // Avanzo ambos localizadores a la vez.
      loc_c1 = siguiente(loc_c1, c1);
      loc_c2 = siguiente(loc_c2, c2);
    }
    // Si el while se ejecuto completamente, si los localizadores
    // resultantes son == NULL, entonces las cadenas son iguales.
    return (loc_c1 == NULL) && (loc_c2 == NULL) ;
  }
}

void bubble_sort(localizador_t final, cadena_t cad){
  if (inicio_cadena(cad) == final_cadena(cad)){
    // si el final como el inicio apuntan al mismo nodo:
    //  - o bien la cadena es vacia => retorno, no hay nada para ordenar;
    //  - o bien hay un elemento => retorno, no hay nada para ordenar;
    return;
  }
  if (!es_localizador(final)){
    // condicion de parada. Si llegue a NULL de moviendome
    // desde la cola a la cabeza retorno.
    return;
  }

  localizador_t loc = inicio_cadena(cad);
  while (loc != final){
    if (numero_info(info_cadena(loc, cad)) >
        numero_info(info_cadena(siguiente(loc, cad), cad))){
      // Si el dato num siguiente es menor, debo intercambiarlos
      // Ej: [(2, i), (1,j)] => [(1, j), (2, i)]
      intercambiar(loc, siguiente(loc, cad), cad);
    }
    // Sino avanzo al siguiente
    loc = siguiente(loc, cad);
  }
  // llamo recursivamente con el anterior del final, dejo el ultimo elemento
  // fuera, ya que no hay elementos mas grandes que el luego del while.
  bubble_sort(anterior(final, cad), cad);
}


cadena_t concatenar(cadena_t c1, cadena_t c2){
  cadena_t c3 = segmento_cadena(inicio_cadena(c1), final_cadena(c1), c1);
  cadena_t sgm = segmento_cadena(inicio_cadena(c2), final_cadena(c2), c2);
  insertar_segmento_despues(sgm, final_cadena(c3), c3);
  liberar_cadena(sgm);
  return c3;
}

void ordenar(cadena_t &cad){
  if (!es_vacia_cadena(cad)){
    bubble_sort(final_cadena(cad), cad);
  }
}

void cambiar_todos(int original, int nuevo, cadena_t &cad){
  localizador_t loc = siguiente_clave(original, inicio_cadena(cad), cad);
  while (es_localizador(loc)) {
    char *frase_orig = new char[strlen(frase_info(info_cadena(loc, cad))) + 1];
    strcpy(frase_orig, frase_info(info_cadena(loc, cad)));
    info_t tmp = info_cadena(loc, cad);
    cambiar_en_cadena(crear_info(nuevo, frase_orig), loc, cad);
    liberar_info(tmp);
    loc = siguiente_clave(original, loc, cad);
  }
}

cadena_t subcadena(int menor, int mayor, cadena_t cad){
  assert(pertenece(menor, cad) && pertenece(mayor, cad));
  cadena_t ret = crear_cadena();
  // Obtengo el primer loc. donde aparece el menor. Punto de partida
  localizador_t loc = siguiente_clave(menor, inicio_cadena(cad), cad);
  while (es_localizador(loc) && (numero_info(info_cadena(loc, cad)) <= mayor)){
    insertar_al_final(copia_info(info_cadena(loc, cad)), ret);
    loc = siguiente(loc, cad);
  }
  return ret;
}
