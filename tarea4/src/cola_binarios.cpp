/* 5119163 */

/**
@file cola_binarios.cpp
@author Mario Visca
@date 30-04-2018
*/
#include <stdlib.h>
#include <assert.h>

#include "../include/cola_binarios.h"

struct nodo_binario {
  binario_t dato;
  nodo_binario *sig;
};

struct rep_cola_binarios {
  nodo_binario *ppio;
  nodo_binario *final;
};

cola_binarios_t crear_cola_binarios(){
  cola_binarios_t ret = new rep_cola_binarios;
  ret->ppio = ret->final = NULL;
  return ret;
}

void encolar(binario_t b, cola_binarios_t &c){
  nodo_binario *nodo = new nodo_binario;
  nodo->dato = b;
  nodo->sig = NULL;

  if (es_vacia_cola_binarios(c)){
    c->ppio = c->final = nodo;
  } else{
    c->final->sig = nodo;
    c->final = nodo;
  }
}

void desencolar(cola_binarios_t &c){
  if (!es_vacia_cola_binarios(c)){
    nodo_binario *tmp = c->ppio;
    c->ppio = c->ppio->sig;
    delete tmp;
    if (c->ppio == NULL){
      c->final = NULL;
    }
  }
}

void liberar_cola_binarios(cola_binarios_t &c){
  while (c->ppio != NULL){
    nodo_binario *aux = c->ppio;
    c->ppio = c->ppio->sig;
    delete aux;
  }
  c->final = NULL;
  delete c;
  c = NULL;
}

bool es_vacia_cola_binarios(cola_binarios_t c){
  return (c->ppio == NULL);
}

binario_t frente(cola_binarios_t c){
  assert(!es_vacia_cola_binarios(c));
  return (c->ppio->dato);
}
