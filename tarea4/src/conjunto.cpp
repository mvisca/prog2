/* 5119163 */

/**
@file conjunto.cpp
@author Mario Visca
@date 30-04-2018
*/

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "../include/conjunto.h"
#include "../include/binario.h"
#include "../include/cola_binarios.h"

struct rep_conjunto{
  binario_t ABB;
};


conjunto_t crear_conjunto(){
  conjunto_t ret = new rep_conjunto;
  ret->ABB = NULL;
  return ret;
}


conjunto_t construir_conjunto(cadena_t cad){ // O(n)
  conjunto_t ret = new rep_conjunto;
  ret->ABB = crear_balanceado(cad); // O(n)
  return ret;
}

bool incluir(info_t i, conjunto_t &c){// O(log n)
  bool ret = insertar_en_binario(i, c->ABB); // O(log n)
  return ret;
}

void excluir(char *t, conjunto_t &c){// O(log n)
  remover_de_binario(t, c->ABB);// O(log n)
}

void liberar_conjunto(conjunto_t &c){// O(n)
  liberar_binario(c->ABB);
  delete c;
  c = NULL;
}

bool pertenece_conjunto(char *t, conjunto_t c){// O(log n)
  bool ret;
  ret = (buscar_subarbol(t, c->ABB) != NULL);
  return ret;
}

bool es_vacio_conjunto(conjunto_t c){// O(1)
  bool ret = (c == NULL || es_vacio_binario(c->ABB));
  return ret;
}

static void encolar_en_orden(cola_binarios_t &c, binario_t abb){// O(n)
  if(abb == NULL){
    return;
  }else{
    encolar_en_orden(c, izquierdo(abb));
    encolar(abb, c);
    encolar_en_orden(c, derecho(abb));
  }
}

conjunto_t union_conjunto(conjunto_t s1, conjunto_t s2){
  cola_binarios_t c1 = crear_cola_binarios(); //O(1)
  cola_binarios_t c2 = crear_cola_binarios(); //O(1)

  encolar_en_orden(c1, s1->ABB); //O(n1)
  encolar_en_orden(c2, s2->ABB); //O(n2)

  cadena_t tmp = crear_cadena();  //O(1)
  while (!es_vacia_cola_binarios(c1) && !es_vacia_cola_binarios(c2)){ //O(k), k=min(n1, n2)
   binario_t frente_c1 = frente(c1); //O(1)
   binario_t frente_c2 = frente(c2); //O(1)
    if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) == 0){ //O(1)
      insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
      desencolar(c1); //O(1)
      desencolar(c2); //O(1)
    }else{
      if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) < 0){ //O(1)
        insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
        desencolar(c1); //O(1)
      }else{
        insertar_al_final(copia_info(raiz(frente_c2)), tmp); //O(1)
        desencolar(c2); //O(1)
      }
    }
  }
  while(!es_vacia_cola_binarios(c1)){
    binario_t frente_c1 = frente(c1); //O(1)
    insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
    desencolar(c1);
  }
  while(!es_vacia_cola_binarios(c2)){
    binario_t frente_c2 = frente(c2); //O(1)
    insertar_al_final(copia_info(raiz(frente_c2)), tmp); //O(1)
    desencolar(c2);
  }

  conjunto_t s3 = construir_conjunto(tmp); //O(n)
  liberar_cadena(tmp); //O(n)
  liberar_cola_binarios(c1);
  liberar_cola_binarios(c2);
  return s3;
}

conjunto_t interseccion(conjunto_t s1, conjunto_t s2){
  cola_binarios_t c1 = crear_cola_binarios(); //O(1)
  cola_binarios_t c2 = crear_cola_binarios(); //O(1)

  encolar_en_orden(c1, s1->ABB); //O(n1)
  encolar_en_orden(c2, s2->ABB); //O(n2)

  cadena_t tmp = crear_cadena();  //O(1)
  while (!es_vacia_cola_binarios(c1) && !es_vacia_cola_binarios(c2)){ //O(k), k=min(n1, n2)
   binario_t frente_c1 = frente(c1); //O(1)
   binario_t frente_c2 = frente(c2); //O(1)
    if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) == 0){ //O(1)
      insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
      desencolar(c1); //O(1)
      desencolar(c2); //O(1)
    }else{
      if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) < 0){ //O(1)
        desencolar(c1); //O(1)
      }else{
        desencolar(c2); //O(1)
      }
    }
  }

  conjunto_t s3 = construir_conjunto(tmp); //O(n)
  liberar_cadena(tmp); //O(n)
  liberar_cola_binarios(c1);
  liberar_cola_binarios(c2);

  return s3;
}

conjunto_t diferencia(conjunto_t s1, conjunto_t s2){// O(n1 +n2 + n)
  cola_binarios_t c1 = crear_cola_binarios(); //O(1)
  cola_binarios_t c2 = crear_cola_binarios(); //O(1)

  encolar_en_orden(c1, s1->ABB); //O(n1)
  encolar_en_orden(c2, s2->ABB); //O(n2)

  cadena_t tmp = crear_cadena();  //O(1)
  while (!es_vacia_cola_binarios(c1) && !es_vacia_cola_binarios(c2)){ //O(k), k=min(n1, n2)
   binario_t frente_c1 = frente(c1); //O(1)
   binario_t frente_c2 = frente(c2); //O(1)
    if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) == 0){ //O(1)
      desencolar(c1); //O(1)
      desencolar(c2); //O(1)
    }else{
      if (strcmp(frase_info(raiz(frente_c1)), frase_info(raiz(frente_c2))) < 0){ //O(1)
        insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
        desencolar(c1); //O(1)
      }else{
        desencolar(c2); //O(1)
      }
    }
  }
  while(!es_vacia_cola_binarios(c1)){
    binario_t frente_c1 = frente(c1); //O(1)
    insertar_al_final(copia_info(raiz(frente_c1)), tmp); //O(1)
    desencolar(c1);
  }

  conjunto_t s3 = construir_conjunto(tmp); //O(n)
  liberar_cadena(tmp); //O(n)
  liberar_cola_binarios(c1); // O(n1)
  liberar_cola_binarios(c2); // O(n2)
  return s3;
}


static void cnj2iter(iterador_t &it, binario_t b){// O(n)
  if (b == NULL){
    return;
  }
  cnj2iter(it, izquierdo(b));
  char *tmp =  new char[strlen(frase_info(raiz(b))) + 1];
  strcpy(tmp, frase_info(raiz(b)));
  agregar_a_iterador(tmp, it);
  cnj2iter(it, derecho(b));
  return;
}

iterador_t iterador_conjunto(conjunto_t c){// O(n)
  iterador_t ret = crear_iterador();
  cnj2iter(ret, c->ABB);
  return ret;
}
