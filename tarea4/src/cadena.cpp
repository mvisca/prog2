/* 5119163 */

/**
@file cadena.cpp
@author Mario Visca
@date 30-04-2018
*/

#include "../include/cadena.h"
#include "../include/info.h"

#include <stddef.h>
#include <stdio.h>
#include <assert.h>

struct nodo {
  info_t dato;
  nodo *anterior;
  nodo *siguiente;
};

struct rep_cadena {
  nodo *inicio;
  nodo *final;
};

cadena_t crear_cadena(){// O(1)
  cadena_t res = new rep_cadena;
  res->inicio = res->final = NULL;
  return res;
}

void insertar_al_final(info_t i, cadena_t &cad) {// O(1)
  nodo *nuevo = new nodo;
  nuevo->dato = i;
  nuevo->siguiente = NULL;
  nuevo->anterior = cad->final;
  if (cad->final == NULL) {
    assert(cad->inicio == NULL);
    cad->inicio = nuevo;
  } else {
    assert(cad->inicio != NULL);
    cad->final->siguiente = nuevo;
  }
  cad->final = nuevo;
}

void insertar_antes(info_t i, localizador_t loc, cadena_t &cad){// O(1)
  assert(localizador_en_cadena(loc, cad));
  nodo *nuevo = new nodo;
  nuevo->dato = i;
  nuevo->siguiente = loc;
  nuevo->anterior = loc->anterior;
  loc->anterior = nuevo;
  if (nuevo->anterior == NULL){
    cad->inicio = nuevo;
  } else {
    nuevo->anterior->siguiente = nuevo;
  }
}

void insertar_segmento_despues(cadena_t &sgm, localizador_t loc, cadena_t &cad){// O(1)
  assert(es_vacia_cadena(cad) || localizador_en_cadena(loc, cad));
  if (es_vacia_cadena(cad)){
    cad->inicio = sgm->inicio;
    cad->final = sgm->final;
  } else {
    if (!es_vacia_cadena(sgm)){
      sgm->inicio->anterior = loc;
      sgm->final->siguiente = loc->siguiente;
      if (es_final_cadena(loc, cad))
        cad->final = sgm->final;
      else
        loc->siguiente->anterior = sgm->final;

      loc->siguiente = sgm->inicio;
    }
  }
  sgm->inicio = sgm->final = NULL;
}

cadena_t segmento_cadena(localizador_t desde, localizador_t hasta, cadena_t cad){// O(n)
  assert(es_vacia_cadena(cad) || precede_en_cadena(desde, hasta, cad));
  cadena_t res = crear_cadena();
  if (!es_vacia_cadena(cad)){
    localizador_t loc = desde;
    while (loc != siguiente(hasta, cad)){
      //ERROR: compartiria  memoria
      //info_t info = loc->dato
      info_t info = copia_info((loc->dato));
      insertar_al_final(info, res);
      loc = siguiente(loc, cad);
    }
  }
  return  res;
}

cadena_t separar_segmento(localizador_t desde, localizador_t hasta, cadena_t &cad){// O(1)
  assert(es_vacia_cadena(cad) || precede_en_cadena(desde, hasta, cad));
  cadena_t res = crear_cadena();
  if (es_vacia_cadena(cad)){
    return res;
  } else {// O(1)
    if (es_inicio_cadena(desde, cad) && es_final_cadena(hasta, cad)){// O(1)
      cad->inicio = cad->final = NULL;
    } else if (!es_inicio_cadena(desde, cad) && es_final_cadena(hasta, cad)){// O(1)
      cad->final = desde->anterior;
      desde->anterior->siguiente = hasta->siguiente;
      desde->anterior = NULL;

    } else if (es_inicio_cadena(desde, cad) && !es_final_cadena(hasta, cad)){// O(1)
      cad->inicio = hasta->siguiente;
      hasta->siguiente->anterior = desde->anterior;
      hasta->siguiente = NULL;

    } else {// O(1)
      desde->anterior->siguiente = hasta->siguiente;
      hasta->siguiente->anterior = desde->anterior;

      desde->anterior = hasta->siguiente = NULL;
    }
  }
  res->inicio = desde;
  res->final = hasta;

  return res;
}

void _copy_recursive(localizador_t loc, cadena_t &dest){// O(n)
  // si es distinto de NULL, => hay un nodo a copiar
  if (loc != NULL){
    nodo *to_copy = new nodo;
    to_copy->dato = copia_info((loc->dato));
    // Caso base cadena vacia:
    if (es_vacia_cadena(dest)){
      to_copy->siguiente = to_copy -> anterior = NULL;
      dest->inicio = dest->final = to_copy;
    } else {
      // inserto al final
      to_copy->siguiente = NULL;
      to_copy->anterior = dest->final;
      dest->final->siguiente = to_copy; // Enlazo con el nuevo ultimo.
      dest->final = to_copy; // Actualizo el final.
    }
    _copy_recursive(loc->siguiente, dest);
  }
}

cadena_t copiar_cadena(cadena_t cad){// O(n)
  cadena_t dest = crear_cadena();
  _copy_recursive(cad->inicio, dest); // O(n)
  return dest;
}

cadena_t mezcla(cadena_t c1, cadena_t c2){// O(n)
  assert(esta_ordenada(c1) && esta_ordenada(c2));
  cadena_t c3;

  if (es_vacia_cadena(c1)){// O(n)
    c3 = copiar_cadena(c2);
  } else if (es_vacia_cadena(c2)){// O(n)
    c3 = copiar_cadena(c1);
  } else{// O(n)
    c3 = crear_cadena();
    localizador_t loc_c1, loc_c2;
    loc_c1 = inicio_cadena(c1);
    loc_c2 = inicio_cadena(c2);
    while (es_localizador(loc_c1) && es_localizador(loc_c2)){// O(k), k=min(n1, n2)
      if (numero_info(loc_c1->dato) == numero_info(loc_c2->dato)){
        insertar_al_final(copia_info(info_cadena(loc_c1, c1)), c3);
        loc_c1 = siguiente(loc_c1, c1);
      } else {
        if (numero_info(loc_c1->dato) < numero_info(loc_c2->dato)){
          insertar_al_final(copia_info(info_cadena(loc_c1, c1)), c3);
          loc_c1 = siguiente(loc_c1, c1);
        } else {
          insertar_al_final(copia_info(info_cadena(loc_c2, c2)), c3);
          loc_c2 = siguiente(loc_c2, c2);
        }
      }
    }

    while (es_localizador(loc_c1)){ // O(n1 -k)
      insertar_al_final(copia_info(info_cadena(loc_c1, c1)), c3);
      loc_c1 = siguiente(loc_c1, c1);
    }

    while (es_localizador(loc_c2)){// O(n2 - k)
      insertar_al_final(copia_info(info_cadena(loc_c2, c2)), c3);
      loc_c2 = siguiente(loc_c2, c2);
    }
    }
  return c3;
}

void remover_de_cadena(localizador_t &loc, cadena_t &cad){// O(1)
  assert(localizador_en_cadena(loc, cad));

  if (es_inicio_cadena(loc, cad) && es_final_cadena(loc, cad)){
    // es el unico elemento
    cad->inicio = cad->final = NULL;
  } else {
    if (loc->anterior == NULL){
      // es el primer elemento
      loc->siguiente->anterior = NULL;
      cad->inicio = loc->siguiente;
    } else if (loc->siguiente == NULL){
      // es el ultimo elemento
      loc->anterior->siguiente = NULL;
      cad->final = loc->anterior;
    } else{
      // no estoy en los bordes
      loc->anterior->siguiente = loc->siguiente;
      loc->siguiente->anterior = loc->anterior;
    }

  }
  liberar_info(loc->dato);
  delete loc;
  loc = NULL;
}

void liberar_cadena(cadena_t &cad){// O(n)
  nodo *a_borrar;
  while (cad->inicio != NULL) {
    a_borrar = cad->inicio;
    cad->inicio = cad->inicio->siguiente;
    liberar_info(a_borrar->dato);
    delete (a_borrar);
  }
  delete cad;
}

bool es_localizador(localizador_t loc){ // O(1)
  return loc != NULL;
}

bool es_vacia_cadena(cadena_t cad){// O(1)
  return (cad->inicio == NULL) && (cad->final == NULL);
}

bool esta_ordenada(cadena_t cad){// O(n)
  bool res = true;
  if (!es_vacia_cadena(cad)){
    localizador_t loc = inicio_cadena(cad);
    while (res && es_localizador(siguiente(loc, cad))){
      localizador_t sig_loc = siguiente(loc, cad);
      if (numero_info(info_cadena(loc, cad)) >
          numero_info(info_cadena(sig_loc, cad)))
        res = false;
      else
        loc = siguiente(loc, cad);
    }
  }
  return res;
}

bool es_inicio_cadena(localizador_t loc, cadena_t cad){// O(1)
  bool ret;
  if (es_vacia_cadena(cad)){
    ret = false;
  } else {
    ret =  loc == cad->inicio;
  }
  return ret;
}

bool es_final_cadena(localizador_t loc, cadena_t cad){// O(1)
  bool ret;
  if (es_vacia_cadena(cad)){
    ret = false;
  } else {
    ret =  loc == cad->final;
  }
  return ret;
}

bool localizador_en_cadena(localizador_t loc, cadena_t cad){// O(n)
  bool ret = false;
  if (es_vacia_cadena(cad) || !es_localizador(loc)){
    return ret;
  } else {
    localizador_t cursor = inicio_cadena(cad);
    while (es_localizador(cursor) && cursor != loc){
      cursor = cursor->siguiente;
    }
    ret = cursor == loc;
  }
  return ret;
}

bool precede_en_cadena(localizador_t loc1, localizador_t loc2, cadena_t cad){// O(n)
  localizador_t cursor = loc1;
  while (es_localizador(cursor) && (cursor != loc2))
    cursor = siguiente(cursor, cad);
  return ((cursor == loc2) && (localizador_en_cadena(loc1, cad)));
}

localizador_t inicio_cadena(cadena_t cad){// O(1)
  localizador_t loc;
  if (es_vacia_cadena(cad)){
    loc = NULL;
  } else {
    loc = cad->inicio;
  }
  return loc;
}

localizador_t final_cadena(cadena_t cad){// O(1)
  localizador_t loc;
  if (es_vacia_cadena(cad)){
    loc = NULL;
  } else {
    loc = cad->final;
  }
  return loc;
}

localizador_t kesimo(nat k, cadena_t cad){// O(n)
  localizador_t loc;
  if ( (k == 0) || (es_vacia_cadena(cad))){
    loc = NULL;
  } else {
    loc = cad->inicio;
    unsigned int cant_elem = 1;
    while ((loc != NULL) && (cant_elem < k)){
      loc = loc->siguiente;
      cant_elem++;
    }
    if (cant_elem < k){
      loc = NULL;
    }
  }

  return loc;
}

localizador_t siguiente(localizador_t loc, cadena_t cad){// O(1)
  assert(localizador_en_cadena(loc, cad));
  localizador_t res;
  if (es_final_cadena(loc, cad)){
    res = NULL;
  } else {
    res = loc->siguiente;
  }
  return res;
}

localizador_t anterior(localizador_t loc, cadena_t cad){// O(1)
  assert(localizador_en_cadena(loc, cad));
  localizador_t res;
  if (es_inicio_cadena(loc, cad)){
    res = NULL;
  } else {
    res = loc->anterior;
  }
  return res;
}

localizador_t menor_en_cadena(localizador_t loc, cadena_t cad){// O(n)
  assert(localizador_en_cadena(loc, cad));
  localizador_t res = loc;
  while (es_localizador(siguiente(loc, cad))){
    loc = siguiente(loc, cad);
    if (numero_info(info_cadena(loc, cad)) < numero_info(info_cadena(res, cad)))
      res = loc;
  }
  return res;
}

localizador_t siguiente_clave(int clave, localizador_t loc, cadena_t cad){// O(n)
  assert(es_vacia_cadena(cad) || localizador_en_cadena(loc, cad));
  localizador_t res = loc;
  if (es_vacia_cadena(cad))
    res = NULL;
  else {
    while (es_localizador(res) && numero_info(info_cadena(res, cad)) != clave)
      res = siguiente(res, cad);
  }
  return res;
}

localizador_t anterior_clave(int clave, localizador_t loc, cadena_t cad){// O(n)
  assert(es_vacia_cadena(cad) || localizador_en_cadena(loc, cad));
  localizador_t res = loc;
  if (es_vacia_cadena(cad))
    res = NULL;
  else {
    while (es_localizador(res) && numero_info(info_cadena(res, cad)) != clave)
      res = anterior(res, cad);
  }
  return res;
}

info_t info_cadena(localizador_t loc, cadena_t cad){// O(1)
  assert(localizador_en_cadena(loc, cad));
  return (loc->dato);
}

void cambiar_en_cadena(info_t i, localizador_t loc, cadena_t &cad){// O(1)
  assert(localizador_en_cadena(loc, cad));
  loc->dato = i;
}

void intercambiar(localizador_t loc1, localizador_t loc2, cadena_t &cad){// O(1)
  assert(localizador_en_cadena(loc1, cad) && localizador_en_cadena(loc2, cad));
  info_t aux;
  aux = loc1->dato;
  loc1->dato = loc2->dato;
  loc2->dato = aux;
}

void _print_recursive(localizador_t loc){// O(n)
  if (loc == NULL){
    printf("\n");
  } else {
      printf("(%d,%s)", numero_info(loc->dato), frase_info(loc->dato));
      _print_recursive(loc->siguiente);
  }
}

void imprimir_cadena(cadena_t cad){// O(n)
  if (es_vacia_cadena(cad)){
    printf("\n");
  } else {
    _print_recursive(cad->inicio);// O(n)
  }
}
